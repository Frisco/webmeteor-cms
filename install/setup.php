<?php 
/*-------------------------------------------------------+
| METEOR CMS
| Copyright (C) 2012-2015 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: install/setup.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
error_reporting(E_ALL);
?>
<!DOCTYPE HTML>

<html lang="de">
<head>
<meta charset="utf-8">
<title>METEOR CMS Installation Schritt 1</title>
<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/cufon-replace.js" type="text/javascript"></script>
<script src="js/Mate_400.font.js" type="text/javascript"></script>
<script src="js/FF-cash.js" type="text/javascript"></script>
<!--[if lt IE 7]> <div style=' clear: both; height: 59px; padding:0 0 0 15px; position: relative;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0014_german.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
<!--[if lt IE 9]>
			<script type="text/javascript" src="js/html5.js"></script>
			<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->

<link rel="apple-touch-icon" sizes="57x57" href="../ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../ico/-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="../ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="../ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="../ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="../ico/favicon-16x16.png">
<link rel="manifest" href="../ico//manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="../ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
<body id="page3">
<div class="extra">
  <div class="main"> 
    <!--==============================header=================================-->
    <header>
      <div class="wrapper p4">
        <h1><a href="">METEOR CMS</a></h1>
      </div>
      <nav>
        <ul class="menu">
          <li><a href="http://www.webmeteor24.de" target="_blank">WebMeteor24 Home</a></li>
          <li><a href="http://www.webmeteor24.de/kontakt" target="_blank">Installationsservice</a></li>
          <li><a href="http://www.webmeteor24.de/download" target="_blank">Downloads</a></li>
          <li class="last"><a href="http://www.webmeteor24.de/forum" target="_blank">Forum</a></li>
        </ul>
      </nav>
    </header>
    <!--==============================content================================-->
    <section id="content">
      <div class="container_12">
        <div class="wrapper">
          <article class="grid_8">
            <div class="indent-left2 indent-top">
              <h3 class="p1">Meteor CMS Installation Schritt 1</h3>
              <?php 
          $err='';  
			$array = apache_get_modules(); 
if (!in_array("mod_rewrite", $array)) {
 $err.= "<p style='color:red'>Leider ist Mod_Rewrite auf Ihrem Webhosting/Server nicht verf&uuml;gbar! Bitte wenden Sie sich an Ihren Webhoster.</p>";
} 

if (!function_exists('filter_list')) {
  $err.="<p style='color:red'>Leider ist die auf Ihrem Webhosting/Server installierte PHP Version zu alt! Es wird PHP ab Version 5.x dringend ben&ouml;tigt! Bitte wenden Sie sich an Ihren Webhoster.</p>";
}
chmod('../images',0777);
chmod('../config.php',0777);

if(!is_writable('../config.php')){
	$err.="<p style='color:red'>Leider ist die Datei config.php nicht beschreibbar. Bitte ändern Sie den CHMOD auf 0777 mit Ihrem FTP Programm.</p>";
}
if(!is_writable('../images')){
	$err.="<p style='color:red'>Leider ist der Ordner images nicht beschreibbar. Bitte ändern Sie den CHMOD auf 0777 mit Ihrem FTP Programm.</p>";
}

if($err!=''){
	echo "<h6 class='p1'>Folgende Probleme sind aufgetreten!</h6>";
	echo $err;
	echo "<div class='aligncenter'>
			<a class='button' href='setup.php'>Nochmal pr&uuml;fen</a>
		  </div>
";
}else{
	
            ?>
              <p>Vielen Dank, dass Sie sich f&uuml;r das METEOR CMS entschieden haben.</p>
              <p>Wir haben bei der Entwicklung darauf geachtet alles so einfach wie m&ouml;glich zuhalten. Leider funktioniert die Installation aber nicht ganz ohne Ihre Hilfe.</p>
              <p>In diesem ersten Installations Schritt ben&ouml;tigt das METEOR CMS die Daten f&uuml;r die MySQL Datenbank. Diese haben Sie entweder von Ihrem Hoster bekommen oder k&ouml;nnen diese bei Ihrem Hoster im Kundenbereich entnehmen. Eine Datenbank sollte vorher installiert sein.</p>
              <p>Sollten Probleme bei der Installation auftauchen, so wenden Sie sich Bitte an <a href='http://www.webmeteor24.de' target="_blank">WebMeteor24</a>. Dort wird Ihnen dann gerne weiter geholfen. Gerne k&ouml;nnen Sie dort auch den Installationsservice beauftragen. Der Installationsservice kostet Sie 10,-&euro; inklusive 19% MwSt.. Dabei werden dann auch gleich die globalen Einstellungen &uuml;bernommen.</p>
              <hr>
              <form id="database" action="step2.php" method="post">
                <fieldset id="form">
                  <label><span class="text-form">Datenbankhost:</span>
                    <input type="text" name="dbhost" value="localhost">
                    <span class="form-desc">(in der Regel localhost)</span></label>
                  <label><span class="text-form">Datenbankname:</span>
                    <input type="text" name="dbname">
                    <span class="form-desc">(der Name der Datenbank)</span></label>
                  <label><span class="text-form">Datenbankbenutzername:</span>
                    <input type="text" name="dbuser">
                    <span class="form-desc">(Der Benutzername f&uuml;r die Datenbank)</span></label>
                  <label><span class="text-form">Datenbankpasswort:</span>
                    <input type="text" name="dbpass">
                    <span class="form-desc">(Das Passwort f&uuml;r den Datenbankzugriff)</span></label>
                  <label><span class="text-form">Pr&auml;fix:</span>
                    <input type="text" name="dbpref" value="meteor_">
                    <span class="form-desc">(wird den Datenbanktabellen vorangestellt, kann frei gew&auml;hlt werden, es d&uuml;rfen keine Umlaute oder Sonderzeichen verwendet werden)</span></label>
                  <div class="clear"></div>
                  <div class="buttons"> <a class="button" href="#" onClick="document.getElementById('database').reset()">zur&uuml;cksetzen</a> <a class="button" href="#" onClick="document.getElementById('database').submit()">speichern</a> </div>
                </fieldset>
              </form>
              <?php
}
?>
            </div>
          </article>
          <article class="grid_4">
            <div class="box-2">
              <div class="padding">
                <h3>Unsere Empfehlungen</h3>
                <p class="border-bot p2">Meteor CMS ist getestet auf dem kostenlosen Webspace von <a href="http://www.lima-city.de/homepage/ref:268057" target="_blank">Lima-City</a></p>
              </div>
            </div>
          </article>
        </div>
      </div>
      <div class="block"></div>
    </section>
  </div>
</div>
<!--==============================footer=================================-->
<footer>
  <div class="main">
    <div class="inner">
      <p>WebMeteor24 &copy; 2015 </p>
      <p>Meteor CMS ist lizensiert unter <a class="link" href="http://www.gnu.org/licenses/gpl.html" target="_blank" rel="nofollow">GNU General Public License Version 3</a></p>
    </div>
  </div>
</footer>
<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>