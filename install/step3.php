<?php 
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: install/step3.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
error_reporting(E_ALL);
if(isset($_POST['email'])){
	//schauen ob alles ausgefüllt
	if($_POST['email']!='' && $_POST['username']!='' && $_POST['userpass']!=''){
		include '../config.php';
		$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_name);
		mysqli_set_charset($mysqli, "utf8");
		/*
 		* Use this instead of $connect_error if you need to ensure
 		* compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 		*/
		if (mysqli_connect_error()) {
    		die('<div style="font-family:Verdana;font-size:11px;text-align:center;"><b>Unable to establish connection to MySQL</b><br /> (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
		}
		$result=dbquery("INSERT INTO ".DB_PREFIX."user (user_name, user_pass, user_rechte, user_email, user_angemeldet, user_status) VALUES ('".clean($_POST['username'],'sql')."', '".hash( 'sha512', trim( $_POST['userpass'] ).$salt )."', '103', '".clean($_POST['email'],'sql')."', '".date('Y-m-d H:i:s')."', '0')");
		$result=dbquery("UPDATE ".DB_PREFIX."settings SET settings_wert='".clean($_POST['email'],'sql')."' WHERE settings_name='siteemail'");
	}
?>

<!DOCTYPE HTML>

<html lang="de">
<head>
<meta charset="utf-8">
<title>Meteor CMS Installation Schritt 3</title>
<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/cufon-replace.js" type="text/javascript"></script>
<script src="js/Mate_400.font.js" type="text/javascript"></script>
<script src="js/FF-cash.js" type="text/javascript"></script>
<!--[if lt IE 7]> <div style=' clear: both; height: 59px; padding:0 0 0 15px; position: relative;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0014_german.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
<!--[if lt IE 9]>
			<script type="text/javascript" src="js/html5.js"></script>
			<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->
</head>
<body id="page3">
<div class="extra">
  <div class="main"> 
    <!--==============================header=================================-->
    <header>
      <div class="wrapper p4">
        <h1><a href="">Meteor CMS</a></h1>
      </div>
     	<nav>
        <ul class="menu">
          <li><a href="http://www.webmeteor24.de" target="_blank">WebMeteor24 Home</a></li>
          <li><a href="http://www.webmeteor.de/kontact" target="_blank">Installationsservice</a></li>
          <li><a href="http://www.webmeteor24.de/downloads" target="_blank">Downloads</a></li>
          <li class="last"><a href="http://www.webmeteor.de/forum" target="_blank">Forum</a></li>
        </ul>
      </nav>
    </header>
    <!--==============================content================================-->
    <section id="content">
      <div class="container_12">
        <div class="wrapper">
          <article class="grid_8">
            <div class="indent-left2 indent-top">
              <h3 class="p1">Meteor CMS Installation letzter Schritt</h3>
              <p>Gratulation, Meteor CMS ist fertig installiert.</p>
              <p>Aus Sicherheitsgründen löschen Sie Bitte den Ordner install über Ihr FTP-Programm.</p>
              <p><?
			  if(is_writeable('../config.php')){
				  ?>
                  <p>Bitte setzen Sie den CHMOD der config.php auf 0600 mit Ihrem FTP-Programm.</p>
                  <? } ?>
              <p>Nun k&ouml;nnen Sie sich in Ihren <a href='../admin/'>Adminbereich</a> einloggen und die globalen Einstellungen bearbeiten und Ihre Inhalte einpflegen.</p>
            </div>
          </article>
          <article class="grid_4">
            <div class="box-2">
              <div class="padding">
                <h3>Unsere Empfehlungen</h3>
                <p class="border-bot p2">Meteor CMS ist getestet auf dem kostenlosen Webspace von <a href="http://www.lima-city.de/homepage/ref:268057" target="_blank">Lima-City</a></p>
              </div>
            </div>
          </article>
        </div>
      </div>
      <div class="block"></div>
    </section>
  </div>
</div>
<!--==============================footer=================================-->
<footer>
  <div class="main">
    <div class="inner">
      <p>WebMeteor24 &copy; 2012 </p>
      <p>Meteor CMS ist lizensiert unter <a class="link" href="http://www.gnu.org/licenses/gpl.html" target="_blank" rel="nofollow">GNU General Public License Version 3</a></p>
    </div>
  </div>
</footer>
<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>
<?php 

}else{
	header('Location: setup.php');
}

function dbquery($query) {
	global $mysqli;

		if(empty($query)) return false;
		$result = $mysqli->query($query) or die();
		if (!$result) {
			echo $mysqli->error;
			return false;
		}
		return $result;
}

function clean($string,$type){
	global $mysqli;
	switch ($type){
		case "int":
		return filter_var(filter_var($string, FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);
		case "string":
		return filter_var($string, FILTER_SANITIZE_STRING);
		case "sql":
		return mysqli_real_escape_string($mysqli,$string);
		break;
		case 'nohtml': // Will convert both double and single quotes.
        return htmlentities($string, ENT_QUOTES, "UTF-8");
        break;
        case 'plain': // Will leave both double and single quotes unconverted
        return htmlentities($string,ENT_NOQUOTES, "UTF-8");
        break;
        case 'upper_word': // trim string, upper case words
        return ucwords(strtolower(trim($string)));
        break;
        case 'ucfirst': // trim string, upper case first word
        return ucfirst(strtolower(trim($string)));
        break;
        case 'lower': // trim string, lower case words
        return strtolower(trim($string));
        break;
        case 'urlencode': // trim string, url encoded
        return urlencode(trim($string));
        break;
		case 'urldecode': // trim string, url decoded
        return urldecode(trim($string));
        break;
		case "email":
		return filter_var(filter_var($string, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
		break;
		case "url":
		return filter_var(filter_var($string, FILTER_SANITIZE_URL), FILTER_VALIDATE_URL);
		break;
		case "ip":
		return filter_var(filter_var($string, FILTER_SANITIZE_IP), FILTER_VALIDATE_IP);
		break;
		default:
		return 0;
		break;
        }
}
?>