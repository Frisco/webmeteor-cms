<?php 
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: install/step2.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
error_reporting(E_ALL);
//schauen ob vom Schritt 1 gekommen
if(isset($_POST['dbhost'])){
	//schauen ob alles ausgefüllt
	if($_POST['dbhost']!='' && $_POST['dbname']!='' && $_POST['dbpass']!='' && $_POST['dbuser']!='' && $_POST['dbpref']!=''){
		
		//config.php schreiben 
		
		$dbhost = $_POST['dbhost'];
		$dbuser = $_POST['dbuser'];
		$dbpass = $_POST['dbpass'];
		$dbname = $_POST['dbname'];
		$dbpref = $_POST['dbpref'];
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		mysqli_set_charset($mysqli, "utf8");
		/*
 		* Use this instead of $connect_error if you need to ensure
 		* compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 		*/
		if (mysqli_connect_error()) {
    		die('<div style="font-family:Verdana;font-size:11px;text-align:center;"><b>Unable to establish connection to MySQL</b><br /> (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
		}
		
		if($mysqli){
			$salt=createRandomPrefix(32);
			
			$config = "<?php\n";
			$config .= "/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: config.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/\n";
			$config .= "$"."db_host = "."\"".$_POST['dbhost']."\";\n";
			$config .= "$"."db_user = "."\"".$_POST['dbuser']."\";\n";
			$config .= "$"."db_pass = "."\"".$_POST['dbpass']."\";\n";
			$config .= "$"."db_name = "."\"".$_POST['dbname']."\";\n";
			$config .= "$"."dbpref = "."\"".$dbpref."\";\n";
			$config .= "$"."salt = "."\"".$salt."\";\n";
			$config .= "define("."\""."DB_PREFIX"."\"".", "."\"".$dbpref."\");\n";
			$config .= "?>";
			$temp = fopen("../config.php","w");
			fwrite($temp, $config); 
			fclose($temp);
			chmod("../config.php",0600);
			
			// Datenbank Tabellen erstellen

			$result = dbquery("CREATE TABLE ".$dbpref."settings (
  				settings_name varchar(200) NOT NULL DEFAULT '',
  				settings_wert TEXT,
				UNIQUE (settings_name)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;");
		
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('sitename', 'Dein Seitenname')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('siteemail', 'Deine Emailadresse')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('siteowner', 'Dein Name')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('description', 'Kurze Beschreibung der Seite')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('slogan', 'Slogan Deiner Webseite')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('keywords', 'Keywörter')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('footer', 'Eigener Seitenfooter')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('design', 'default')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('editor', '1')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('email_footer', 'Email Signatur')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('email_header', 'Dein Seitenname')");
			$result = dbquery("INSERT INTO ".$dbpref."settings (settings_name, settings_wert) VALUES ('startpage', 'seite_1_home.html')");

			$result = dbquery("CREATE TABLE ".$dbpref."navigation (
  				nav_id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  				nav_name varchar(100) NOT NULL DEFAULT '',
  				nav_url varchar(250) NOT NULL DEFAULT '',
  				nav_visibility tinyint(3) unsigned NOT NULL DEFAULT '0',
  				nav_position tinyint(1) unsigned NOT NULL DEFAULT '1',
  				nav_order smallint(2) unsigned NOT NULL DEFAULT '1',
				nav_target VARCHAR(10) NOT NULL DEFAULT '',
  				PRIMARY KEY (nav_id)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");


			$result = dbquery("INSERT INTO ".$dbpref."navigation (nav_name, nav_url, nav_visibility, nav_position, nav_order) VALUES
('Home',  'seite_1_home.html', 0, 0, 1)");
			$result = dbquery("INSERT INTO ".$dbpref."navigation (nav_name, nav_url, nav_visibility, nav_position, nav_order) VALUES
('Kontakt',  'kontakt.html', 0, 0, 2)");


			$result = dbquery("CREATE TABLE ".$dbpref."page (
				page_id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
				page_title VARCHAR(200) NOT NULL DEFAULT '',
				page_access TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
				page_content TEXT NOT NULL,
				page_meta_desc TEXT NOT NULL,
				page_meta_key TEXT NOT NULL,
				page_allow_comments TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
				page_allow_ratings TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
				PRIMARY KEY (page_id)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");
			
			$result = dbquery("INSERT INTO ".$dbpref."page (page_title, page_access, page_content, page_meta_desc, page_meta_key, page_allow_comments, page_allow_ratings) VALUES
('Home',  '0', 'Das ist der Startseitentext, den Sie im Adminbereich ändern können', 'Die Beschreibung dazu', 'keywords, seo, cms,content management system', 0 , 0)");
			
			$result = dbquery("CREATE TABLE ".$dbpref."kontakt (
				page_id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
				page_title VARCHAR(200) NOT NULL DEFAULT '',
				page_access TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
				page_content TEXT NOT NULL,
				page_meta_desc TEXT NOT NULL,
				page_meta_key TEXT NOT NULL,
				danke_content TEXT NOT NULL,
				danke_mail TEXT NOT NULL,
				PRIMARY KEY (page_id)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");
			
			$result = dbquery("INSERT INTO ".$dbpref."kontakt (page_title, page_access, page_content, page_meta_desc, page_meta_key, danke_content, danke_mail) VALUES
('Kontakt',  '0', 'Sie k&ouml;nnen mich &uuml;ber die Kontaktadresse im Impressum erreichen, oder Sie f&uuml;llen dieses Formular aus. Nach dem Absenden erhalte ich dann Ihre Kontaktanfrage. Sie erhalten diese dann ebenfalls f&uuml;r Ihre Unterlagen, an Ihre hier angegebene E-Mail Adresse.', 'Kontakt', 'Kontaktformular, Kontaktanfrage, Support', 'Vielen Dank f&uuml;r Ihre Kontaktanfrage', 'Vielen Dank f&uuml;r Ihre Kontaktanfrage');");
		
			$result = dbquery("CREATE TABLE ".$dbpref."user (
				userid MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
				user_name VARCHAR(20) NOT NULL DEFAULT '',
				user_pass VARCHAR(128) NOT NULL DEFAULT '',
				user_email VARCHAR(50) NOT NULL DEFAULT '',
				user_session VARCHAR(50) NOT NULL DEFAULT '',
				user_rechte INT (8) UNSIGNED NOT NULL DEFAULT '100',
				user_angemeldet DATETIME,
				user_online DATETIME,
				user_status TINYINT(1) unsigned NOT NULL DEFAULT '1',
				activation_key VARCHAR(128) NOT NULL DEFAULT '',
				PRIMARY KEY (userid),
				UNIQUE (user_name, user_email)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");
			
			$result = dbquery("CREATE TABLE ".$dbpref."admin_nav (
				admin_nav_id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
				admin_nav_name VARCHAR(50) NOT NULL DEFAULT '',
				admin_nav_datei VARCHAR(50) NOT NULL DEFAULT '',
				admin_nav_cat TINYINT (2) UNSIGNED NOT NULL DEFAULT '0',
				PRIMARY KEY (admin_nav_id),
				UNIQUE (admin_nav_name, admin_nav_datei)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");
			
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Seite erstellen/ ändern', 'seite.php', '1')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Navigation', 'links.php', '1')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Kontaktformular', 'kontakt.php', '4')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Haupteinstellungen', 'settings.php', '4')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Boxen', 'box.php', '4')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Erweiterungen installieren', 'erweiterungen.php', '5')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('System Update', 'update.php', '5')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Profilfelder Kategorien', 'profilfelder_kategorien.php', '2')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Profilfelder', 'profilfelder.php', '2')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav (admin_nav_name, admin_nav_datei, admin_nav_cat) VALUES ('Mitglieder Übersicht', 'mitglieder.php', '2')");
			
			$result = dbquery("CREATE TABLE ".$dbpref."admin_nav_cat (
				admin_nav_cat_id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
				admin_nav_cat_name VARCHAR(50) NOT NULL DEFAULT '',
				PRIMARY KEY (admin_nav_cat_id),
				UNIQUE (admin_nav_cat_name)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav_cat (admin_nav_cat_name) VALUES ('Content')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav_cat (admin_nav_cat_name) VALUES ('Mitglieder')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav_cat (admin_nav_cat_name) VALUES ('Statistiken')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav_cat (admin_nav_cat_name) VALUES ('Einstellungen')");
			$result = dbquery("INSERT INTO ".$dbpref."admin_nav_cat (admin_nav_cat_name) VALUES ('Sonstiges')");
			
			$result = dbquery("CREATE TABLE ".$dbpref."box (
  				box_id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  				box_name varchar(100) NOT NULL DEFAULT '',
				box_title varchar(100) NOT NULL DEFAULT '',
  				box_url varchar(250) NOT NULL DEFAULT '',
  				box_visibility tinyint(3) unsigned NOT NULL DEFAULT '0',
  				box_anzeige tinyint(2) unsigned NOT NULL DEFAULT '1',
  				box_order smallint(2) unsigned NOT NULL DEFAULT '1',
				box_content TEXT,
				box_page TEXT,
  				PRIMARY KEY (box_id)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");
			
			$result = dbquery("CREATE TABLE ".$dbpref."safe_pages (
				safe_pages_id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
				safe_page VARCHAR(50) NOT NULL DEFAULT '',
				PRIMARY KEY (safe_pages_id),
				UNIQUE (safe_page)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");
			$result = dbquery("INSERT INTO ".$dbpref."safe_pages (safe_page) VALUES ('seite')");
			$result = dbquery("INSERT INTO ".$dbpref."safe_pages (safe_page) VALUES ('kontakt')");
			
			$result = dbquery("CREATE TABLE ".$dbpref."erweiterung (
  				erweiterungs_id mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  				erweiterungs_titel varchar(100) NOT NULL DEFAULT '',
				erweiterungs_version varchar(10) NOT NULL DEFAULT '',
  				erweiterungs_entwicklermail varchar(250) NOT NULL DEFAULT '',
  				erweiterungs_entwicklerweb varchar(250) NOT NULL DEFAULT '',
  				erweiterungs_entwickler varchar(250) NOT NULL DEFAULT '',
  				erweiterungs_ordner varchar(250) NOT NULL DEFAULT '',
				erweiterungs_beschreibung TEXT,
  				PRIMARY KEY (erweiterungs_id)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;");
			
			$result = dbquery("CREATE TABLE ".$dbpref."user_field_cats (
				field_cat_id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
				field_cat_name VARCHAR(200) NOT NULL ,
				field_cat_db VARCHAR(100) NOT NULL,
				field_cat_index VARCHAR(200) NOT NULL,
				field_cat_class VARCHAR(50) NOT NULL,
				field_cat_page SMALLINT(1) UNSIGNED NOT NULL DEFAULT '0',
				field_cat_order SMALLINT(5) UNSIGNED NOT NULL ,
				PRIMARY KEY (field_cat_id)
			) ENGINE=MyISAM DEFAULT CHARSET=UTF8 COLLATE=utf8_unicode_ci");
							
							
			$result = dbquery("CREATE TABLE ".$dbpref."user_fields (
				field_id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
				field_name VARCHAR(50) NOT NULL,
				field_cat MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '1',
				field_required TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
				field_log TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
				field_registration TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
				field_order SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
				PRIMARY KEY (field_id),
				KEY field_order (field_order)
			) ENGINE=MyISAM DEFAULT CHARSET=UTF8 COLLATE=utf8_unicode_ci");


			
		}else{
			header('Location: setup.php?err=1');
		}
		  
	}else{
		header('Location: setup.php?err=2');
	}
?>


<!DOCTYPE HTML>

<html lang="de">
<head>
<meta charset="utf-8">
<title>Meteor CMS Installation Schritt 2</title>
<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/grid.css" type="text/css" media="screen">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/cufon-replace.js" type="text/javascript"></script>
<script src="js/Mate_400.font.js" type="text/javascript"></script>
<script src="js/FF-cash.js" type="text/javascript"></script>
<!--[if lt IE 7]> <div style=' clear: both; height: 59px; padding:0 0 0 15px; position: relative;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0014_german.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
<!--[if lt IE 9]>
			<script type="text/javascript" src="js/html5.js"></script>
			<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
		<![endif]-->
</head>
<body id="page3">
<div class="extra">
  <div class="main"> 
    <!--==============================header=================================-->
    <header>
      <div class="wrapper p4">
        <h1><a href="">Meteor CMS</a></h1>
      </div>
      <nav>
        <ul class="menu">
          <li><a href="http://www.webmeteor24.de" target="_blank">WebMeteor24 Home</a></li>
          <li><a href="http://www.webmeteor24.de/kontakt" target="_blank">Installationsservice</a></li>
          <li><a href="http://www.webmeteor24.de/download" target="_blank">Downloads</a></li>
          <li class="last"><a href="http://www.webmeteor24.de/forum" target="_blank">Forum</a></li>
        </ul>
      </nav>
    </header>
    <!--==============================content================================-->
    <section id="content">
      <div class="container_12">
        <div class="wrapper">
          <article class="grid_8">
            <div class="indent-left2 indent-top">
              <h3 class="p1">Meteor CMS Installation Schritt 2</h3>
<p>Die config.php wurde geschrieben und die Datenbank wurde angelegt.</p>
<p>In diesem letzten Schritt ben&ouml;tigt das Meteor CMS noch einen Benutzernamen, Ihre E-Mailadresse und ein Passwort, damit Sie in den Adminbereich kommen, um Ihre Webseite anzulegen und zu pflegen.</p>
<p>Sollten Probleme bei der Installation auftauchen, so wenden Sie sich Bitte an das Forum auf <a href='http://www.webmeteor24.de' target="_blank">WebMeteor24</a>. Dort wird Ihnen dann weiter geholfen. Gerne k&ouml;nnen Sie dort auch den Installationsservice beauftragen.</p>
<hr />
<form id="database" action="step3.php" method="post">
<fieldset id="form">
                  <label><span class="text-form">E-Mailadresse:</span>
                    <input type="text" name="email"></label>
                    <label><span class="text-form">Benutzername:</span>
                    <input type="text" name="username"></label>
                    <label><span class="text-form">Passwort:</span>
                    <input type="password" name="userpass"></label>
                    
<div class="clear"></div>
                  <div class="buttons"> <a class="button" href="#" onClick="document.getElementById('database').reset()">zur&uuml;cksetzen</a> <a class="button" href="#" onClick="document.getElementById('database').submit()">speichern</a> </div>
                </fieldset>
</form>
 </div>
          </article>
          <article class="grid_4">
            <div class="box-2">
              <div class="padding">
                <h3>Unsere Empfehlungen</h3>
                <p class="border-bot p2">Meteor CMS ist getestet auf dem kostenlosen Webspace von <a href="http://www.lima-city.de/homepage/ref:268057" target="_blank">Lima-City</a></p>
              </div>
            </div>
          </article>
        </div>
      </div>
      <div class="block"></div>
    </section>
  </div>
</div>
<!--==============================footer=================================-->
<footer>
  <div class="main">
    <div class="inner">
      <p>WebMeteor24 &copy; 2015 </p>
      <p>Meteor CMS ist lizensiert unter <a class="link" href="http://www.gnu.org/licenses/gpl.html" target="_blank" rel="nofollow">GNU General Public License Version 3</a></p>
    </div>
  </div>
</footer>
<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>
<?php 
/*}else{
	header('Location: setup.php');
}*/
}
function dbquery($query) {
	global $mysqli;

		if(empty($query)) return false;
		$result = $mysqli->query($query) or die();
		if (!$result) {
			echo $mysqli->error;
			return false;
		}
		return $result;
}

// Generate a random string
function createRandomPrefix($length = 5) {
	$chars = array("abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ", "123456789");
	$count = array((strlen($chars[0])-1), (strlen($chars[1])-1));
	$prefix = "";
	for ($i = 0; $i < $length; $i++) {
		$type = mt_rand(0, 1);
		$prefix .= substr($chars[$type], mt_rand(0, $count[$type]), 1);
	}
	return $prefix;
}
?>