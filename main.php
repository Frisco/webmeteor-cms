<?php
/*-------------------------------------------------------+
| METEOR CMS
| Copyright (C) 2012-2015 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: main.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/

if (preg_match("/main.php/i", $_SERVER['PHP_SELF']))
{
	die('Direct access not allowed!');
}

/** Level off Error Reporting 0= Off, E_ALL= Development Mode **/
//error_reporting(0);
error_reporting(E_ALL);

define("DEBUG", "ON"); //ON = Development Mode, OFF Produce Mode

if(function_exists('ini_set')){
if (DEBUG == "ON"){
	ini_set('display_errors', 'On');
}else{
	ini_set('display_errors', 'Off');
	//ini_set('log_errors', 'On');
	//ini_set('error_log', 'fehler.txt');
}
/** Set memory limit to work with **/
@ini_set('memory_limit', '32M');
}

/** Set Default date_default_timezone **/
if (function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
	@date_default_timezone_set(@date_default_timezone_get());

/** Set Encofing for MB-Funktions **/
mb_internal_encoding("utf-8");

// Calculate script start/end time
function get_microtime() {
	list($usec, $sec) = explode(" ", microtime());
	return ((float)$usec+(float)$sec);
}

// Define script start time
define("START_TIME", get_microtime());

// define is loaded in CMS
define('IN_METEOR', true);

/** Prevent any possible XSS attacks via $_GET. **/
foreach ($_GET as $check_url)
{
	if ((preg_match("/<[^>]*script*\"?[^>]*>/i", $check_url)) || (preg_match("/<[^>]*object*\"?[^>]*>/i",
		$check_url)) || (preg_match("/<[^>]*iframe*\"?[^>]*>/i", $check_url)) || (preg_match("/<[^>]*applet*\"?[^>]*>/i",
		$check_url)) || (preg_match("/<[^>]*meta*\"?[^>]*>/i", $check_url)) || (preg_match("/<[^>]*style*\"?[^>]*>/i",
		$check_url)) || (preg_match("/<[^>]*form*\"?[^>]*>/i", $check_url)) || (preg_match("/\([^>]*\"?[^)]*\)/i",
		$check_url)) || (preg_match("/\"/i", $check_url)))
	{
		die("Hijacking attempt, dying....");
	}
}
unset($check_url);

/** Locate config.php **/
$folder_level = "";
$i = 0;
while (! file_exists($folder_level . "config.php")){
	$folder_level .= "../";
	$i++;
	if ($i == 7){
		die("config.php wurde nicht gefunden!");
	}
}

require_once $folder_level . "config.php";
if(!isset($db_host)) header('location: install/setup.php');

// Initialise session
session_start();
/** Set Encoding **/
header('content-type: text/html; charset=utf-8');

// Common definitions
define("METEOR_REQUEST", isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != "" ? $_SERVER['REQUEST_URI'] : $_SERVER['SCRIPT_NAME']);
define("METEOR_QUERY", isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "");
define("METEOR_SELF", basename($_SERVER['PHP_SELF']));
define("METEOR_IP", $_SERVER['REMOTE_ADDR']);
define("QUOTES_GPC", (ini_get('magic_quotes_gpc') ? TRUE : FALSE));
$times = substr_count($_SERVER['PHP_SELF'],"/");

// Load Database Table Definitions
require_once('inc/db_defines.php');
// PDO database functions
$_mysql_querys = array();
require_once 'inc/dbhandler.php';

// Establish mySQL database connection
$link = dbconnect($db_host, $db_user, $db_pass, $db_name);
unset($db_host, $db_user, $db_pass);

// Fetch the Site Settings from the database and store them in the $settings variable
$settings = array();
$result = dbquery("SELECT settings_name, settings_wert FROM ".DB_SETTINGS);
while ($data = dbarray($result)){
	$settings[$data['settings_name']] = $data['settings_wert'];
}

// Define Rootfolder
define('METEOR_ROOT', str_replace('\\', '/', realpath(dirname(__FILE__))) .'/');

// Load Folder definitions
require_once('inc/folder_defines.php');
// Load Design Informations
require_once('design/'.$settings['design'].'/design_info.php');

// Initialise userdata
$userdata=array();

//Login
if(isset($_POST['user_name']) && isset($_POST['user_password']) && $_POST['user_name'] != '' && $_POST['user_password'] != ''){
   $userid=check_user($_POST['user_name'], hash( 'sha512', trim( $_POST['user_password'] ).$salt ));
   if ($userid!=false){
	   login($userid);
	   header('location:'.$_SERVER['PHP_SELF']);
   }
}
//logout
if(isset($_POST['logout'])){
	logout();
	header('location:'.$_SERVER['PHP_SELF']);
}

//Userdaten laden
if (logged_in()){

   $select_userdata = dbquery("SELECT * FROM ".DB_USER."
    WHERE user_session='".session_id()."'
    LIMIT 1");
   $userdata=dbarray($select_userdata);
   
   $sql="UPDATE ".DB_USER."
    SET user_online='".date('Y-m-d H:i:s')."'
    WHERE userid='".$userdata['userid']."'";
     dbquery($sql);
	 
}else{
	$userdata = array("user_rechte" => 0);
}

// User level, Admin Rights & User Group definitions
define("iGUEST", $userdata['user_rechte'] == 0 ? 1 : 0);
define("iMEMBER", $userdata['user_rechte'] >= 101 ? 1 : 0);
define("iADMIN", $userdata['user_rechte'] >= 102 ? 1 : 0);
define("iSUPERADMIN", $userdata['user_rechte'] == 103 ? 1 : 0);

/*if (!isset($_COOKIE['visited'])) {
	$result = dbquery("UPDATE ".DB_SETTINGS." SET settings_wert=".$settings['counter']."+1 WHERE settings_name='counter'");
	setcookie("visited", "yes", time() + 31536000, "/", "", "0");
}*/
// Stops Header Output
ob_start();

//Page Navigation
function makepagenav($start, $count, $total, $range=0, $link = ""){
global $locale;
if ($link == "") $link = METEOR_SELF."_";
$res="";
$pg_cnt=ceil($total / $count);
if ($pg_cnt > 1) {
$idx_back = $start - $count;
$idx_next = $start + $count;
$cur_page=ceil(($start + 1) / $count);
//$res.= $locale['PAGE']." ".$cur_page.$locale['PAGE_OF'].$pg_cnt.": ";
if ($idx_back >= 0) {
//$res.="<a class='pagination' href='".$link."rowstart=".$idx_back."'>".$locale['PAGE_BACK']."</a>\n";
$res.="<a class='pagination' href='".$link."_".$idx_back."'>Zur&uuml;ck</a>\n";
if ($cur_page > ($range + 1)) $res.="<a class='pagination' href='".$link."_0'>1</a>...\n";
}
$idx_fst=max($cur_page - $range, 1);
$idx_lst=min($cur_page + $range, $pg_cnt);
if ($range==0) {
$idx_fst = 1;
$idx_lst=$pg_cnt;
}
for($i=$idx_fst;$i<=$idx_lst;$i++) {
$offset_page=($i - 1) * $count;
if ($i==$cur_page) {
$res.="<a class='current'><b>".$i."</b></a>\n";
} else {
$res.="<a class='pagination' href='".$link."_".$offset_page."'>".$i."</a>\n";
}
}
if ($idx_next < $total) {
if ($cur_page < ($pg_cnt - $range)) $res.="... <a class='pagination' href='".$link."_".($pg_cnt-1)*$count."'>".$pg_cnt."</a>\n";
//$res.="<a class='pagination' href='".$link."rowstart=".$idx_next."'>".$locale['PAGE_VORWARD']."</a>\n";
$res.="<a class='pagination' href='".$link."_".$idx_next."'>Vor</a>\n";
}
return "<div class='pagenav'>\n".$res."</div>\n";
}
}


/** Trim a line of text to a preferred length **/
function trimText($text, $length)
{
	$dec = array("\"", "'", "\\", '\"', "\'", "<", ">");
	$enc = array("&quot;", "&#39;", "&#92;", "&quot;", "&#39;", "&lt;", "&gt;");
	$text = str_replace($enc, $dec, $text);
	if (strlen($text) > $length)
		$text = substr($text, 0, ($length - 3)) . "...";
	$text = str_replace($dec, $enc, $text);
	return $text;
}

function clean($string,$type){
	global $mysqli;
	switch ($type){
		case "int":
		return filter_var(filter_var($string, FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);
		case "string":
		return filter_var($string, FILTER_SANITIZE_STRING);
		case "sql":
		return mysqli_real_escape_string($mysqli,$string);
		break;
		case 'nohtml': // Will convert both double and single quotes.
        return htmlentities($string, ENT_QUOTES, "UTF-8");
        break;
        case 'plain': // Will leave both double and single quotes unconverted
        return htmlentities($string,ENT_NOQUOTES, "UTF-8");
        break;
        case 'upper_word': // trim string, upper case words
        return ucwords(strtolower(trim($string)));
        break;
        case 'ucfirst': // trim string, upper case first word
        return ucfirst(strtolower(trim($string)));
        break;
        case 'lower': // trim string, lower case words
        return strtolower(trim($string));
        break;
        case 'urlencode': // trim string, url encoded
        return urlencode(trim($string));
        break;
		case 'urldecode': // trim string, url decoded
        return urldecode(trim($string));
        break;
		case "email":
		return filter_var(filter_var($string, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
		break;
		case "url":
		return filter_var(filter_var($string, FILTER_SANITIZE_URL), FILTER_VALIDATE_URL);
		break;
		case "ip":
		return filter_var(filter_var($string, FILTER_SANITIZE_IP), FILTER_VALIDATE_IP);
		break;
		default:
		return 0;
		break;
        }
}

function stripfilename($filename) {
	$filename = strtolower(str_replace(" ", "_", $filename));
	$filename = preg_replace("/[^a-zA-Z0-9_-]/", "", $filename);
	$filename = preg_replace("/^\W/", "", $filename);
	$filename = preg_replace('/([_-])\1+/', '$1', $filename);
	if ($filename == "") { $filename = time(); }

	return $filename;
}

// Scan image files for malicious code
function verify_image($file) {
	$txt = file_get_contents($file);
	$image_safe = true;
	if (preg_match('#<?php#i', $txt)) { $image_safe = false; } //edit
	elseif (preg_match('#&(quot|lt|gt|nbsp|<?php);#i', $txt)) { $image_safe = false; }
	elseif (preg_match("#&\#x([0-9a-f]+);#i", $txt)) { $image_safe = false; }
	elseif (preg_match('#&\#([0-9]+);#i', $txt)) { $image_safe = false; }
	elseif (preg_match("#([a-z]*)=([\`\'\"]*)script:#iU", $txt)) { $image_safe = false; }
	elseif (preg_match("#([a-z]*)=([\`\'\"]*)javascript:#iU", $txt)) { $image_safe = false; }
	elseif (preg_match("#([a-z]*)=([\'\"]*)vbscript:#iU", $txt)) { $image_safe = false; }
	elseif (preg_match("#(<[^>]+)style=([\`\'\"]*).*expression\([^>]*>#iU", $txt)) { $image_safe = false; }
	elseif (preg_match("#(<[^>]+)style=([\`\'\"]*).*behaviour\([^>]*>#iU", $txt)) { $image_safe = false; }
	elseif (preg_match("#</*(applet|link|style|script|iframe|frame|frameset)[^>]*>#i", $txt)) { $image_safe = false; }
	return $image_safe;
}
// Profil-Link
function profile_link($userid, $user_name, $user_online){
	
	$link="<span ";
	$expire_stamp = date('Y-m-d H:i:s', strtotime("-5 min"));
	$now_stamp    = date("Y-m-d H:i:s", strtotime($user_online));
	if($now_stamp>=$expire_stamp) $link.= "class='online'";
	$link.="><a href='profil_".$userid."_".$user_name."'>".$user_name."</a></span>";
	return $link;
}

//Login
 function check_user($name, $pass){
    $sql="SELECT userid
    FROM ".DB_USER."
    WHERE user_name='".$name."' AND user_pass='".$pass."' 
    LIMIT 1";
    $result= dbquery($sql);
    if ($result){
        $user=dbarray($result);
        return $user['userid'];
    }else
        return false;
}

function login($userid){
    session_regenerate_id(true);
    $sql="UPDATE ".DB_USER."
    SET user_session='".session_id()."'
    WHERE userid='".$userid."'";
     dbquery($sql);
}

function logged_in(){
    $sql="SELECT userid
    FROM ".DB_USER."
    WHERE user_session='".session_id()."'
    LIMIT 1";
    $result= dbquery($sql);
      return (dbrows($result)==1);
}

function logout(){
    $sql="UPDATE ".DB_USER."
    SET user_session='0'
    WHERE user_session='".session_id()."'";
    dbquery($sql);
	session_unset();
    session_destroy();
    session_write_close();
    setcookie(session_name(),'',0,'/');
    session_regenerate_id(true);
}

function copyright(){
	global $settings;
	$copy=date('Y');
	echo "<p class='copyright'>Copyright &copy; ".$copy." ".$settings['sitename'].". Powered by <a href='http://www.webmeteor24.de' target='_blank'>Meteor CMS</a>&nbsp;&nbsp;&nbsp;Meteor CMS is licensed under <a href='http://www.gnu.org/licenses/' target='_blank'>GPLv3</a></p>\n";
}
?>