<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: seiten/kontakt.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../index.php'); }


$err = '';
$err_name    = '';
$err_email   = '';
$err_comment = '';
$name    = '';
$email   = '';
$comment = '';

if(isset($params[1])&&$params[1]=='send'){
	if($_POST['name']==''){
		$err=1;
		$err_name = "<span style='color:red' id='fehlername'>&nbsp;Bitte den Vor und Nachnamen eingeben</span>";
	}else{
		$name=filter_input(INPUT_POST,"name",FILTER_SANITIZE_STRING);
	}
	if(filter_has_var(INPUT_POST,"email")){
		if(!filter_input(INPUT_POST,"email", FILTER_VALIDATE_EMAIL)){
			$err_email = "<span style='color:red' id='fehleremail'>Es ist ein Fehler in Ihrer E-Mailadresse.</span>";
			$err=1;
		}else{
			$email=filter_input(INPUT_POST,"email",FILTER_SANITIZE_EMAIL);
		}
	}else{
		$err_email = "<span style='color:red' id='fehleremail'>Bitte die Emailadresse eingeben</span>";
		$err=1;
	}
	if($_POST['comment']==''){
		$err=1;
		$err_comment = "<span style='color:red' id='fehlercomment'>&nbsp;Bitte Schreiben Sie uns Ihr anliegen.</span>";
	}else{
		$comment=$_POST['comment'];
	}
	
	$result=dbquery("SELECT * FROM ".DB_PREFIX."kontakt");
	$pagedata=dbarray($result);
	
	if($err==''){
		require_once(INC.'sendmail_inc.php');
		// mail an selbst
		$betreff = "Eine neue Kontaktanfrage";
		$message = $name.",\r\nhat folgendes anliegen:\r\n\r\n".$comment."\r\n\r\nKontakt:\r\n\r\n".$email;
		$send=sendemail($settings['sitename'], $settings['siteemail'], $name, $email, $betreff, $message);
		// Dankemail
		//$betreff_besteller = $settings['email_header'];
		$betreff_besteller = "Vielen Dank für Ihre Anfrage bei ".$settings['sitename'];
		//$betreff_besteller .= $settings['email_footer'];
		$nachricht = nl2br($pagedata['danke_mail'])."\r\n\r\n".$comment;
		sendemail($name, $email, $settings['sitename'], $settings['siteemail'], $betreff_besteller, $nachricht);
		if($send==TRUE){
			$content=$pagedata['danke_content'];
			$title=$pagedata['page_title'];
			$meta_desc=$pagedata['page_meta_desc'];
   			$keywords=$pagedata['page_meta_key'];
			$headtags='';
			$body='';
   		}else{
			$content = "<span style='color:red' id='fehlercomment'>Es liegt ein Fehler vor, Bitte versuchen Sie es später noch einmal!</span>";
			$title=$pagedata['page_title'];
			$meta_desc=$pagedata['page_meta_desc'];
   			$keywords=$pagedata['page_meta_key'];
			$headtags='';
			$body='';
		}
	}
	
	

}if(!isset($params[1])||$err=='1'){

	$result=dbquery("SELECT * FROM ".DB_PREFIX."kontakt");
	$pagedata=dbarray($result);

   	$title=$pagedata['page_title'];
   	$content=$pagedata['page_content'];
   	$meta_desc=$pagedata['page_meta_desc'];
   	$keywords=$pagedata['page_meta_key'];

	$headtags='<script type="text/javascript">
	<!--
		function checkEntry(element)
		{
			id = element.id;
			if (document.getElementById(id).value.length == 0)
			{
				document.getElementById(id).style.border="solid red 1px";
				document.getElementById(id).outerHTML+=\'<font style="color:red" id="fehler\'+id+\'">&nbsp;Ist ein Pflichtfeld!</font>\';
				return false;
			}
			else
			{
				document.getElementById(id).style.border="solid black 1px";
				return true;
			}
		}
		function delEntry(element){
			id = element.id;
			var comment = document.getElementById("fehler"+id);
			comment.remove();
}
Object.prototype.remove = function() {
    this.parentNode.removeChild(this);
}
		-->
	</script>';
	$body='';
	$content.='<form id="contact-form" method="post" enctype="multipart/form-data" action="kontakt_send.html">
              <fieldset>
                <label><span class="text-form">Name:</span>
                  <input type="text" name="name" id="name" onblur="javascript:checkEntry(this)" title="Bitte den Vor und Nachnamen eingeben" onClick="javascript:delEntry(this)" value="'.$name.'" />'.$err_name.'
                </label>
                <label><span class="text-form">E-mail:</span>
                  <input type="text" name="email" id="email" onblur="javascript:checkEntry(this)" title="Bitte die Emailadresse eingeben" onClick="javascript:delEntry(this)"value="'.$email.'" />'.$err_email.'
                </label>
                <div class="wrapper">
                  <span>Nachricht:</span>
                    <textarea name="comment" id="comment" onblur="javascript:checkEntry(this)" title="Bitte Ihre Nachricht eingeben" onClick="javascript:delEntry(this)">'.$comment.'</textarea>'.$err_comment.'
                </div>
                <div class="buttons"> <input class="submit" type="submit" name="contact_submitted" value="Senden" /> </div>
              </fieldset>
            </form>';
}
?>