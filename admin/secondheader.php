<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/secondheader.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: index'); }

require_once(METEOR_ROOT.'seiten/header_mce.php');

?>
<div class="extra">
  <div class="main"> 
    <!--==============================header=================================-->
    <header>
      <div class="wrapper p4">
        <h1><a href="">Meteor CMS Adminbereich</a></h1>
      </div>
      <nav>
        <ul class="menu">
          <li><a href="http://www.webmeteor24.de" target="_blank">Meteor Home</a></li>
          <li><a href="http://www.webmeteor24.de/kontakt.html" target="_blank">Installationsservice</a></li>
          <li><a href="http://www.webmeteor24.de/download" target="_blank">Downloads</a></li>
          <li class="last"><a href="http://www.webmeteor.de/forum" target="_blank">Forum</a></li>
        </ul>
      </nav>
    </header>
    <!--==============================content================================-->
    <section id="content">
      <div class="container_12">
        <div class="wrapper">
          <article class="grid_8">
            <div class="indent-left2 indent-top">