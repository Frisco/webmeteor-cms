<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/kontakt.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';

if (iADMIN){

$body='';
$headtags='';
$title='Kontaktformular-Admin';

if(isset($_POST['page_title'])){
  //Daten validieren für den Datenbankeintrag
  $page_title=$_POST['page_title'];
  $page_content=$_POST['page_content'];
  $page_meta_desc=$_POST['page_meta_desc'];
  $page_keywords=$_POST['page_keywords'];
  $danke_content=$_POST['danke_content'];
  $danke_mail=$_POST['danke_mail'];
  
  dbquery("UPDATE ".DB_PREFIX."kontakt SET page_title='".$page_title."', page_content='".$page_content."', page_meta_desc='".$page_meta_desc."', page_meta_key='".$page_keywords."', danke_content='".$danke_content."', danke_mail='".$danke_mail."' WHERE page_id='1'");
}

$result=dbquery("SELECT * FROM ".DB_PREFIX."kontakt WHERE page_id='1'");
$pagedata=dbarray($result);

require_once 'secondheader.php';
?>
<h3 class="p1">Kontaktformular Admin</h3>
              <form action='kontakt.php' name='page_form' id="kontakt" method='post'>
                <fieldset id="form">
                  <label><span class="text-form">Überschrift:</span>
                    <input type="text" name="page_title" value="<? echo $pagedata['page_title']; ?>">
                  </label>
                  <div class="text-form">Content:</div>
                  <div class="extra-wrap">
                    <textarea name='page_content' class='input' cols='80' rows='10'><? echo stripslashes($pagedata['page_content']) ?></textarea>
                  </div>
                  <script type="text/javascript">
				CKEDITOR.replace( 'page_content',{
					extraPlugins : 'uicolor',
					uiColor: '#1d1d1d',
					width: '430px',
					toolbar :
					[
					['Source'],
['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
['OrderedList','UnorderedList','-','Blockquote'],
['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
['Link','Unlink','Anchor'],
['Image','Flash','Table','Rule','Smiley','SpecialChar'],

['Style','FontFormat','FontSize'],
['TextColor','BGColor'],
['FitWindow','-','About']
					]
				} );
			</script>
                  <label><span class="text-form">Meta Beschreibung der Kontaktseite:</span>
                    <input type="text" name="page_meta_desc" value="<? echo $pagedata['page_meta_desc']; ?>">
                  </label>
                  <label><span class="text-form">Keywords:</span>
                    <input type="text" name="page_keywords" value="<? echo $pagedata['page_meta_key']; ?>">
                  </label>
                  <div class="text-form">Danke Text:</div>
                  <div class="extra-wrap">
                    <textarea name='danke_content' class='input' cols='95' rows='10' style='width:98%'><? echo stripslashes($pagedata['danke_content']) ?></textarea>
                  </div>
                  <script type="text/javascript">
				CKEDITOR.replace( 'danke_content',{
					extraPlugins : 'uicolor',
					uiColor: '#1d1d1d',
					width: '430px',
					toolbar :
					[
					['Source'],
['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
['OrderedList','UnorderedList','-','Blockquote'],
['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
['Link','Unlink','Anchor'],
['Image','Flash','Table','Rule','Smiley','SpecialChar'],

['Style','FontFormat','FontSize'],
['TextColor','BGColor'],
['FitWindow','-','About']
					]
				} );
			</script>
                  <div class="text-form">Danke Mail:</div>
                  <div class="extra-wrap">
                    <textarea name='danke_mail' class='input' cols='95' rows='10' style='width:98%'><? echo nl2br(stripslashes($pagedata['danke_mail'])) ?></textarea>
                  </div>
                  <div class="clear"></div>
                  <div class="buttons"> <a class="button" href="#" onClick="document.getElementById('kontakt').submit()">speichern</a> </div>
                </fieldset>
              </form>

          <?php
	require_once 'footer.php';

}else{
	header('location: index.php');
}
?>