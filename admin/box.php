<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2015 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/box.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';

if (iADMIN){

$body='';
$headtags='';
$title=' - Box';

require_once 'secondheader.php';
require_once '../design/'.$settings['design'].'/design_info.php';

if(!isset($_GET['action'])){ $_GET['action']='';}
switch($_GET['action']) {

    case "insert":
	Insert();
	break;
	
	case "bearbeiten":
	Bearbeiten($_GET['id']);
	break;
	
	case "delete":
	Delete($_GET['id']);
	break;
	
	case "order":
	Order($_GET['id']);
	break;
	
	case "update":
	Update($_GET['id']);
	break;
	
	default:
    Uebersicht();
	break;
}
require_once 'footer.php';

}else{
	header('location: index.php');
}

function Uebersicht(){
	
	require_once '../inc/file.func.php';
    
	$boxdata = dbquery("SELECT * FROM ".DB_PREFIX."box ORDER BY box_anzeige, box_order ASC");
			
			echo "
 <table align='center' cellpadding='0' cellspacing='0' class='main' style='width:660px'>
<tr>
<td>Position:</td>
<td>Reihenfolge:</td>
<td>Boxname:</td>
<td>Optionen:</td>
</tr>

			";
			
			  while ($box = dbarray($boxdata)){
				if($box['box_id']!=''){
				        echo "<tr><form action='box.php?action=order&id=".$box['box_id']."' name='box_order_form' method='post'>";
						echo "<td>".$box['box_anzeige']."</td>";
					    echo "<td><input type='text' class='input' name='box_order' value='".$box['box_order']."' /></td>";
						echo "<td>".$box['box_name']."</td>";
						echo "<td><a href='box.php?action=bearbeiten&id=".$box['box_id']."'>bearbeiten</a> &nbsp; <a href='box.php?action=delete&id=".$box['box_id']."'>l&ouml;schen</a> &nbsp; <input type='submit' class='button' name='box_order_form' value='Reihenfolge &auml;ndern' /></td>";
						echo "</form></tr>";
	                
			   }
			  }
			  
			  //Boxen auslesen und in Array speichern für die Box Auswahl   
			  $boxes = makefilelist('../box/', ".|..", true);
			
			   echo "
			   </table>
			   ";
			   echo "<hr />";
			   echo "<form action='box.php?action=insert' name='box_form' method='post'>
 <table align='center' cellpadding='0' cellspacing='0' class='main'>
<tr>
<td>Boxname:</td>
<td><input type='text' class='input' name='box_name' value='' /></td>
</tr>
<tr>
<td>Box:</td>
<td><select name='box_url' class='input'>\n";
                 echo makefileopts($boxes);
                 echo "</select></td>
</tr>
<tr>
<td>Boxposition:</td>
<td>".show_design_position()."</td>
</tr>
<tr>
<td>Boxreihenfolge:</td>
<td><input type='text' class='input' name='box_order' value='' /></td>
</tr>
<tr>
<td colspan='2' align='center'><input type='submit' class='button' name='box_form' value='Box anlegen' /></td>
</tr>
</table>
</form>";
}

function Insert(){
	global $mysqli;
   dbquery("INSERT INTO ".DB_PREFIX."box (box_name, box_url, box_anzeige, box_order) VALUES ('".$_POST['box_name']."', '".$_POST['box_url']."', '".$_POST['box_anzeige']."', '".$_POST['box_order']."')");
   header('location: box.php');
}

function Bearbeiten($id){
   $result=dbquery("SELECT * FROM ".DB_PREFIX."box WHERE box_id='".$id."'");
   $boxdata=dbarray($result);
   echo "<form action='box.php?action=update&id=".clean($id,'int')."' name='box_form' method='post'>
 <table align='center' cellpadding='0' cellspacing='0' class='main'>";
 //Boxen auslesen und in Array speichern für die Box Auswahl   
			  $boxes = makefilelist('../box/', ".|..", true);
			
			   echo "<form action='box.php?action=insert' name='box_form' method='post'>
 <table align='center' cellpadding='0' cellspacing='0' class='main'>
<tr>
<td>Boxname:</td>
<td><input type='text' class='input' name='box_name' value='".$boxdata['box_name']."' /></td>
</tr>
<tr>
<td>Box:</td>
<td><select name='box_url' class='input'>\n";
                 echo makefileopts($boxes,$boxdata['box_url']);
                 echo "</select></td>
</tr>
<tr>
<td>Boxposition:</td>
<td>".show_design_position()."</td>
</tr>
<tr>
<td>Boxreihenfolge:</td>
<td><input type='text' class='input' name='box_order' value='".$boxdata['box_order']."' /></td>
</tr>
<tr>
<td colspan='2' align='center'><input type='submit' class='button' name='nav_form' value='Box &auml;ndern' /></td>
</tr>
</table>
</form>";
}

function Delete($id){
   dbquery("DELETE FROM ".DB_PREFIX."box WHERE box_id='".clean($id,'int')."'");
   header('location: box.php');
}

function Order($id){
   dbquery("UPDATE ".DB_PREFIX."box SET box_order='".$_POST['box_order']."' WHERE box_id='".clean($id,'int')."'");
   header('location: box.php');
}

function Update($id){
   dbquery("UPDATE ".DB_PREFIX."box SET box_order='".$_POST['nav_order']."' WHERE box_id='".clean($id,'int')."'");
   dbquery("UPDATE ".DB_PREFIX."box SET box_name='".$_POST['nav_name']."' WHERE box_id='".clean($id,'int')."'");
   dbquery("UPDATE ".DB_PREFIX."box SET box_url='".$_POST['nav_url']."' WHERE box_id='".clean($id,'int')."'");
   dbquery("UPDATE ".DB_PREFIX."box SET box_anzeige='".$_POST['nav_anzeige']."' WHERE box_id='".clean($id,'int')."'");
   header('location: box.php');
}
?>