<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/admin_nav.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../index'); }
?>

<article class="grid_4">
  <div class="box-2">
    <div class="padding">
    <?php
$query2  = dbquery("SELECT admin_nav_cat_name FROM ".DB_PREFIX."admin_nav_cat");
$result = dbrows($query2);

for($i=0;$i<=$result;++$i){
	$query  = dbquery("SELECT admin_nav_cat_name FROM ".DB_PREFIX."admin_nav_cat WHERE admin_nav_cat_id=".$i);
	$query1 = "SELECT * FROM ".DB_PREFIX."admin_nav WHERE admin_nav_cat=".$i." ORDER BY admin_nav_id";
	$rows=dbarray($query);
	echo "<h3>".$rows['admin_nav_cat_name']."</h3>\n";
	$res=dbquery($query1);
	echo "<ul>\n";
	while($data=dbarray($res)){
		if($data['admin_nav_datei']=='update.php') echo ''; // muss noch ein Konzept her
		else echo "<li><a href='".$data['admin_nav_datei']."'>".$data['admin_nav_name']."</a></li>\n";
	}  		
	echo "</ul>\n<br>\n";
}
?>
    </div>
  </div>
</article>
