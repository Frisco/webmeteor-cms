<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2015 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/profilfelder.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';

if (iADMIN){

$body='';
$headtags='';
$title=' - Mitglieder';
$msg='';

require_once 'secondheader.php';

if(isset($_GET['action']) && $_GET['action'] == 'delete'){
	$result=dbquery("DELETE FROM ".DB_USER." WHERE userid='".$_GET['id']."'");
	if($result) $msg='<span class="ok">Mitglied erfolgreich gelöscht</span>';
	else $msg='<span class="notok">Mitglied nicht erfolgreich gelöscht</span>';
}
if(isset($_GET['admin']) && $_GET['action'] == 'admin'){
	$result=dbquery("UPDATE ".DB_USER." SET user_rechte='".$_GET['recht']."' WHERE userid='".$_GET['id']."'");
	if($result) $msg='<span class="ok">Mitglied erfolgreich befördert/degradiert</span>';
	else $msg='<span class="notok">Mitglied nicht befördert/degradiert</span>';
}
// Hier kommt noch die Emal Funktion
/*if(isset($_GET['action']) && $_GET['action'] == 'email'){
	$result=dbquery("SELECT user_name, user_email FROM ".DB_USER." WHERE userid='".$_GET['id']."'");
	if($result) $msg='<span class="ok">Mitglied erfolgreich gelöscht</span>';
	else $msg='<span class="notok">Mitglied nicht erfolgreich gelöscht</span>';
}*/
$result=dbquery("SELECT * FROM ".DB_USER);
echo $msg;
echo "<table>
<tr>
<td>Benutzername</td>
<td>Email</td>
<td>Status</td>
<td>Optionen</td>
</tr>\n";
while($data=dbarray($result)){
	echo "<tr>
	<td>".profile_link($data['userid'], $data['user_name'], $data['user_online'])."</td>
	<td><!--<a href='mitglieder.php?action=email&id=".$data['userid']."'>-->".$data['user_email']."<!--</a>--></td>
	<td>";
	if($data['user_status']=='0') echo 'aktiv';
	elseif($data['user_status']=='1') echo 'nicht aktiviert';
	elseif($data['user_status']=='9') echo 'gebannt';
	echo "</td>
	<td><a href='mitglieder.php?action=delete&id=".$data['userid']."'>löschen</a> ";
	if($data['user_rechte']=='101') echo "<a href='mitglieder.php?action=admin&id=".$data['userid']."&recht=102'>zum Admin befördern</a>";
	if($data['user_rechte']=='102') echo "<a href='mitglieder.php?action=admin&id=".$data['userid']."&recht=101'>zum Admin befördern</a>";
	echo "</td>
	</tr>\n";
	
}
echo "</table>";

require_once 'footer.php';
}else header('location: index.php');
?>