<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2015 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/erweiterung.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';

if (iADMIN){

$body='';
$headtags='';
$title=' - Erweiterungen';

require_once 'secondheader.php';

if(!isset($_GET['action'])){ $_GET['action']='';}
switch($_GET['action']) {

    case "insert":
	Insert();
	break;
	
	case "delete":
	Delete(clean($_GET['id'],'int'));
	break;
	
	default:
    Uebersicht();
	break;
}
require_once 'footer.php';

}else{
	header('location: index.php');
}

function Uebersicht(){
	global $mysqli;
	
	$temp = opendir('../erweiterungen');
	$file_list = array();
	while ($folder = readdir($temp)) {
		if (!in_array($folder, array("..", "."))) {
			if (is_dir('../erweiterungen/'.$folder) && file_exists('../erweiterungen/'.$folder."/info.php")) {
				include '../erweiterungen/'.$folder."/info.php";
				$result = dbquery("SELECT erweiterungs_version FROM ".DB_ERWEITERUNGEN." WHERE erweiterungs_ordner='".$ordner."'");
				if (dbrows($result)) {
					$data = dbarray($result);
					if (version_compare($version, $data['erweiterungs_version'], ">")) {
						$file_list[] = "<option value='".$folder."' style='color:#00F'>".ucwords(str_replace("_", " ", $folder))."</option>\n";
					} else {
						$file_list[] = "<option value='".$folder."' style='color:#3F0'>".ucwords(str_replace("_", " ", $folder))."</option>\n";
					}
				} else {
					$file_list[] = "<option value='".$folder."' style='color:red;'>".ucwords(str_replace("_", " ", $folder))."</option>\n";
				}
				//eventuell zurücksetzen der Variablen aus info.php
			}
		}
	}
	closedir($temp);
	sort($file_list);
	//$result->free();

	echo "<div style='text-align:center'>\n";
	if (count($file_list)) {
		echo "<form name='erweiterungsform' method='post' action='erweiterungen.php?action=insert'>\n";
		echo "<select name='erweiterung' class='input' style='width:200px;'>\n";
		echo "<option>Bitte wählen</option>";
		for ($i = 0; $i < count($file_list); $i++) { echo $file_list[$i]; }
		echo "</select> <input type='submit' name='infuse' value='installieren' class='button' />\n";
		
		echo "<br /><br />\n<span style='color:red;'>installiert</span> ::\n";
		echo "<span style='color:green;'>nicht installiert</span> ::\n";
		echo "<span style='color:blue;'>Update vorhanden</span>\n";
		echo "</form>\n";
	} else {
		echo "<br />Keine Erweiterungen vorhanden<br /><br />\n";
	}
	echo "</div>\n";
	
	if($result=dbquery("SELECT * FROM ".DB_PREFIX."erweiterung")){
		echo "<table cellpadding='0' cellspacing='0'>\n";
		echo "<tr>\n";
		echo "<td>Erweiterung</td>\n";
		echo "<td>Version</td>\n";
		echo "<td>Entwickler</td>\n";
		echo "<td>Kontakt</td>\n";
		echo "<td>Optionen</td>\n";
		echo "</tr>\n";
		while($data=dbarray($result)){
			echo "<tr>\n";
			echo "<td>".$data['erweiterungs_titel']."<br>\n".$data['erweiterungs_beschreibung']."</td>\n";
			echo "<td>".$data['erweiterungs_version']."</td>\n";
			echo "<td>".$data['erweiterungs_entwickler']."</td>\n";
			echo "<td><a href='mailto:".$data['erweiterungs_entwicklermail']."'>Entwicklermail</a><br>\n<a href='http://".$data['erweiterungs_entwicklerweb']."' target='_blank'>Entwicklerweb<a></td>\n";
			echo "<td><a href='erweiterung.php?action=delete&id=".$data['erweiterungs_id']."'>löschen</a></td>\n";
			echo "</tr>\n";
		}
		echo "</table>";
	}else{
		echo "<div>Es sind keine Erweiterungen installiert</div>\n";
	}
			
}

function Insert(){
	
	include '../erweiterungen/'.$_POST['erweiterung']."/info.php";
	include '../erweiterungen/'.$_POST['erweiterung']."/info_db.php";
	$insert=dbquery("INSERT INTO ".DB_PREFIX."erweiterung (erweiterungs_titel, erweiterungs_beschreibung, erweiterungs_version, erweiterungs_entwicklermail, erweiterungs_entwicklerweb, erweiterungs_entwickler, erweiterungs_ordner) VALUES ('".$titel."', '".$beschreibung."', '".$version."', '".$email."', '".$web."', '".$developer."', '".$ordner."')");
	
	header('location: erweiterungen.php?do=1');
	
}

function Delete($id){
	
	$data=dbarray(dbquery("SELECT erweiterungs_ordner FROM ".DB_PREFIX."erweiterung WHERE erweiterungs_id='".$id."'"));
	include '../erweiterungen/'.$data['erweiterungs_ordner']."/info.php";
	include '../erweiterungen/'.$data['erweiterungs_ordner']."/info_delete_db.php";
	dbquery("DELETE FROM ".DB_PREFIX."erweiterung WHERE erweiterungs_id='".$id."'");
	
	header('location: erweiterungen.php?do=2');
	
}
?>