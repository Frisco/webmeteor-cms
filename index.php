<?php
/*-------------------------------------------------------+
| METEOR CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: index.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie k�nnen es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation ver�ffentlicht,
| weitergeben und/oder modifizieren,
| entweder gem�� Version 3 der Lizenz oder (nach Ihrer Option) jeder sp�teren Version.
|
| Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung,
| da� es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT F�R EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once 'main.php';

#remove the directory path we don't want
//$request  = str_replace($settings['folder'], "", $_SERVER['REQUEST_URI']);
$request  = str_replace('.html', "", $_SERVER['REQUEST_URI']);
$request  = str_replace('/', "", $request);
#explode the path by '_'
$params = explode("_", $request);

#keeps users from requesting any file they want
$result=dbquery("SELECT safe_page FROM ".DB_SAFE_PAGES." WHERE safe_page='".$params[0]."'");
  
if($result && file_exists(SEITEN.$params[0].".php")){
	require_once SEITEN.$params[0].".php";
} else {
   header('location: '.$settings['startpage']);
}

define('CONTENT',$content);

require_once INC.'box.inc.php';
define('LEFT_SITE',show_box(1));
define('RIGHT_SITE',show_box(2));

require_once INC.'nav.inc.php';

if($userdata['design']!='' && file_exists(DESIGN.$userdata['design'].'/index.php')) require_once DESIGN.$userdata['design'].'/index.php';
else require_once DESIGN.$settings['design'].'/index.php';

require_once SEITEN.'footer.php';

?>


