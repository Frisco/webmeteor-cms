<?php 
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: inc/nav.inc.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie k�nnen es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation ver�ffentlicht,
| weitergeben und/oder modifizieren,
| entweder gem�� Version 3 der Lizenz oder (nach Ihrer Option) jeder sp�teren Version.
|
| Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung,
| da� es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT F�R EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../index.html'); }

function show_head_nav($ul){
	global $mysqli;
	
	$nav= "";
	
	$result = dbquery("SELECT nav_id, nav_name, nav_url FROM ".DB_PREFIX."navigation WHERE nav_position='0' OR nav_position='1' ORDER BY nav_order ASC");
    if ($result){
		echo "<ul ".$ul.">\n";
		while ($data = dbarray($result)){
			if($data['nav_url']!=''){
				if (strstr($data['nav_url'], "http://") || strstr($data['nav_url'], "https://")) {
					echo "<li class='navitop'><a href='".$data['nav_url']."' target='_blank' title='".$data['nav_name']."' class='nav'>".$data['nav_name']."</a></li>\n";
				}else{
					echo "<li class='navitop'><a href='".$data['nav_url']."' title='".$data['nav_name']."' class='nav'>".$data['nav_name']."</a></li>\n";
				}
			}else{
				echo "<li class='navitop'>".$data['nav_name']."</li>\n";
			}
		}
		echo "</ul>\n";
	}else echo "keine Nav";
	//return $nav;
}

function show_box_nav($ulid){
	global $mysqli;
	
	$nav= "";
	
	$result = dbquery("SELECT nav_id, nav_name, nav_url FROM ".DB_PREFIX."navigation WHERE nav_position=3 ORDER BY nav_order ASC");
    if ($result){
		$nav.="<ul id='".$ulid.">\n";
		while ($data = dbarray($result)){
			
			if($data['nav_url']!=''){
				if (strstr($data['nav_url'], "http://") || strstr($data['nav_url'], "https://")) {
					$nav.="<li class='navibox'><a href='".$data['nav_url']."' target='_blank' title='".$data['nav_name']."' class='navbox'>".$data['nav_name']."</a></li>\n";
				}else{
					$nav.="<li class='navibox'><a href='".$data['nav_url']."' title='".$data['nav_name']."' class='navbox'>".$data['nav_name']."</a></li>\n";
				}
			}else{
				$nav.="<li class='navibox'>".$data['nav_name']."</li>\n";
			}
		}
		$nav.="</ul>\n";
	}
	return $nav;
}

function show_footer_nav($ulid){
	global $mysqli;
	
	$nav= "";
	
	$result = dbquery("SELECT nav_id, nav_name, nav_url FROM ".DB_PREFIX."navigation WHERE nav_position=4 ORDER BY nav_order ASC");
    if ($result){
		$nav.="<ul id='".$ulid.">\n";
		while ($data = dbarray($result)){
			if($data['nav_url']!=''){
				if (strstr($data['nav_url'], "http://") || strstr($data['nav_url'], "https://")) {
					$nav.="<li class='navifooter'><a href='".$data['nav_url']."' target='_blank' title='".$data['nav_name']."' class='navfoot'>".$data['nav_name']."</a></li>\n";
				}else{
					$nav.="<li class='navifooter'><a href='".$data['nav_url']."' title='".$data['nav_name']."' class='navfoot'>".$data['nav_name']."</a></li>\n";
				}
			}else{
				$nav.="<li class='navifooter'>".$data['nav_name']."</li>\n";
			}
		}
		$nav.="</ul>\n";
	}
	return $nav;
}
?>