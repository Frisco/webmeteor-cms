﻿<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: inc/curl.func.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
//falls Zugriff nicht über die Root index.php dann Zugriff verweigern und auf die Startseite umleiten
if (!defined("IN_METEOR")) { header('location: ../'); }

function curlStart($domain, $var = false){
    
    $fp = curl_init($domain);
    
    curl_setopt($fp,CURLOPT_TIMEOUT,20);
    
    curl_setopt($fp,CURLOPT_FAILONERROR,1);
    curl_setopt($fp,CURLOPT_REFERER,$domain);
    curl_setopt($fp,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($fp,CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)');
    
    if($var != false) {
        curl_setopt($fp,CURLOPT_POST,1);
        curl_setopt ($fp, CURLOPT_POSTFIELDS, $var); 
    }
    
    curl_exec($fp);
    
    if(curl_errno($fp) != 0) {
        $send = FALSE;
    } else {
        $send = TRUE;
    }
    
    curl_close($fp);
    
    return $send;
    
}
?>